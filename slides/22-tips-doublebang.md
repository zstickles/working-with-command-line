---

### Last Command

```bash
$ !!
```

Two exclamation-marks are used to reference the last command typed in.
This is useful for running a command, seeing the output, and then
running it again with extra options or follow-up commands.

Can be configured to auto-complete the last command.


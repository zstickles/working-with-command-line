## GIT Commands


### Status

```bash
$ git status
```

Get the current status (recursivley) for this directory.
Includes changed files, staged files, the current branch.
Optionally shows ignored files.

Helpful Options:

- `-s` shorthand status (helpful with `wc`)
- `--ignored` show files that are being ignored by GIT

_Example_: `git status -s`


---

### Tab

Pressing tab will auto-complete file and directory names.
Pressing tab twice will return a list of possible matches.
Some commands will also auto-complete.

In ZSH, auto-complete list will be interactive.


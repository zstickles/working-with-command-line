---

### Esc

Exits "enter" mode and goes into "command" mode.
Similar to emacs and vim.
Allows for faster traversal of the current line.
Can be used to search previously entered commands.

Defaults to emacs key bindings. Can be set to use vim bindings.


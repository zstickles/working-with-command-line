---

### File Pattern Searcher

```bash
$ grep <string> <source>
```

Searches the input (file, directory, output) for
a supplied string.
Alternatives include _awk_, _sed_, _ag_.

Helpful Options:

- `-r` recursive search
- `-n` display resultswith line number
- `-l` display on the files with matches
- `-i` case insensitive searching
- `-A` show number of lines before match
- `-B` show number of lines after match
- `--color=always` highlight the search results

_Example_: `grep -rl "moment" src/components`


---

### Log

```bash
$ git log
```

Shows the commit logs.
Can be passed a file to see the git history for it specifically.
Highly customizable. Can mimic most screens in Bitbucket.

Helpful Options:

- `--oneline` show the commits in a short one-line format
- `--graph` show the commits in a branch graph
- `-n` only show a supplied number of commits
- `--format` specify how the commits should be displayed

_Example_: `git log --oneline -n 10`


---

### Difference

```bash
$ git diff
```

Determines the difference between the current state (or supplied state)
and the head (or supplied state).

Helpful Options:

- `--name-only` only show the file names for the files that are different

_Example_: `git diff --name-only`


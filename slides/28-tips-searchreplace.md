---

### Search and Replace

```bash
$ ^search^replace
```

Allows you to run the last command, but with by replacing part of it.
Helpful if you mis-type something or want to perform a different
operation on the same file.

_Example_:

```bash
$ cat clint/src/components/grid/GridHeader.js
-bash: cat: clint: No such file or directory

$ ^clint^client
```

---

### Remove File(s)

```bash
$ rm <source>
```

Removes a source file (or directory).
Can be passed multiple source files.

Helpful Options:

- `-f` remove without asking for permissions
- `-r` removes files and directories

_Example_: `rm file.txt`


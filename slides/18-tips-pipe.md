## Helpful Tips


### Pipe

```bash
$ [command-1] | [command-2]
```

Pass the output of a command as a parameter for a following command.
Multiple commands can be chained together like this.

_Example_: `git diff --name-only | wc -l`

^ Gets the number of files currently changed


# Working With The Command Line

Helpful commands, tips, and tricks to make your terminal experience more efficient.

```
        (
     '( '      "All aboard the shell train"
    "'  //}
   ( ''"
   _||__ ____ ____ ____
  (o)___)}___}}___}}___}
  'U'0 0  0 0  0 0  0 0 
```

---


---

### Manual

```bash
$ man [command]
```

Shows the manual (if there is one) for a given command.
Shows all the posibile options.


---

### Argument Expand

Using `{` and `}`, you can write long, but repetitive commands
in a much simpler format. All comma separated items in the braces
will be auto-expanded to a command for each item.

Useful for creating or removing multiple files.

_Example_:

```bash
$ touch letters/{a,b,c}.txt
```

Is equivalent to:

```bash
$ touch letters/a.txt && touch letters/b.txt && touch letters/c.txt
```


## General Commands


### Change Directory

```bash
$ cd <directory>
```

Change the current working directory.

_Example_: `cd ~/Documents/gsandf`


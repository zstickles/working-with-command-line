---

### Move File(s)

```bash
$ mv <source> <destination>
```

Moves a source file from one location to another.
Can also be used to rename a file.

_Example_: `mv file.txt ~/Documents`


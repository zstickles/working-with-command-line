---

### Word Count

```bash
$ wc <source>
```

Returns the number of words (or lines) in the supplied source.
The source can be a file or output from the terminal.

Helpful Options:

- `-l` return the number of lines
- `-c` return the number of bytes

_Example_: `wc -l npm-debug.log`


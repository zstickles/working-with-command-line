---

### Wildcard

An astrix (`*`) can be used as a wildcard.
Helpful if you don't want to fully type out a filename.

_Example_:

```bash
$ cat presidents/abraham*
```


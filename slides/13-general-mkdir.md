---

### Make a New Directory

```bash
$ mkdir <dirname>
```

Creates a new directory, given a name.

_Example_: `mkdir client`


---

### Copy File(s)

```bash
$ cp <source> <target>
```

Copies a source file.

_Example_: `cp file.txt newFile.txt`


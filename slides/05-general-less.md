---

### Less (is more)

```bash
$ less <file>
```

Opens a file, files, or other output in an interactive view.
Can be used to scroll through files in a view-only mode (similar to vim).
Man pages use this by default.

Helpful Navigation Commands:

- `q` quit
- `j` scroll down a line
- `k` scroll up a line
- `d` scroll down a page
- `u` scroll up a page
- `/` search for a word
    - `n` next result
    - `p` previous result
- `:` command mode
    - `n` next file
    - `p` previous file


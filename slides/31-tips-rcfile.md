---

### User Config Files

Shells allow for a configuration file that the user can create
and use to define custom variables, functions, and custom settings.

For BASH, the file is `~/.bash_profile`.
For ZSH, the file is `~/.zshrc`.
Other hipster shells have a similar file.

Useful for setting aliases for commonly run programs.

_Example_:

```bash
alias dc="docker-compose"

alias search="grep -rn --color=always"
```


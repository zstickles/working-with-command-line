---

### Display a File

```bash
$ tail <source>
```

Displays the last few lines of a file.
Can be used to "watch" the output of a file (useful for logging).

Helpful Options:

- `-f` keep displaying new lines for the file
- `-n` specify the number of lines to show

_Example_: `tail -f npm-debug.log`


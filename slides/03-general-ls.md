---

### List Directory Contents

```bash
$ ls
```

Lists the contents of a directory (current directory if no directory provided).

Helpful Options:

- `-l` long format
- `-a` include hidden files
- `-h` used with the long format to pretty print file sizes
- `-1` force input to be one per line

_Example_: `ls -lah ~/Documents/gsandf`


---

### Create a New File

```bash
$ touch <filename>
```

Creates a new file, given a name.

_Example_: `touch src/components/Header.js`


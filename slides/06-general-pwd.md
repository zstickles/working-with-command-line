---

### Working Directory

```bash
$ pwd
```

Gets the full path of the current working directory


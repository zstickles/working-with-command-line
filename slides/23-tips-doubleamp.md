---

### Multiple Commands

```bash
$ [command-1] && [command-2]
```

Run a command and then run another one after it's complete.
Can run as many commands as needed on one line.
This is syncronous.


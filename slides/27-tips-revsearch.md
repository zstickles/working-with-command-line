---

### Reverse Search

Hitting `control-r` will enter into reverse search mode.
As you enter text, the shell will show the last command that
matches that search. You can hit `control-r` to see more results.

Press enter to run the command, or an arrow key to insert it in the
shell without running.

Very useful way to run long commands you don't fully remember.


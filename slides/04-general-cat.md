---

### Concatination

```bash
$ cat <file>
```

Prints out a file to STDOUT.
If more than one file is supplied, concatinates and prints files.

_Example_: `cat ./Readme.md`


---

### Up

Pressing up will enter the last entered command.
You can continue to press up to see more search results.
You can press down to go down the list.


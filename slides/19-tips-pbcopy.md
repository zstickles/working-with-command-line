---

### Copy to Clipboard

```bash
$ pbcopy [output]
```

**MAC ONLY** copies supplied output to the system clipboard.
Useful with pipes.

_Example_: `cat package.json | pbcopy`

^ Copies the package.json file to the clipboard


---

### Insert Previous Arguments

```bash
$ !$
```

An exclamation followed by a dollar sign will insert the arguments
from the last command. This works like the double exclamation mark.
Can be set to auto-expand.

Adding two exclamation marks will do the arguments for the second-
to-last command.


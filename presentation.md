# Working With The Command Line

Helpful commands, tips, and tricks to make your terminal experience more efficient.

```
        (
     '( '      "All aboard the shell train"
    "'  //}
   ( ''"
   _||__ ____ ____ ____
  (o)___)}___}}___}}___}
  'U'0 0  0 0  0 0  0 0 
```

---

## General Commands


### Change Directory

```bash
$ cd <directory>
```

Change the current working directory.

_Example_: `cd ~/Documents/gsandf`

---

### List Directory Contents

```bash
$ ls
```

Lists the contents of a directory (current directory if no directory provided).

Helpful Options:

- `-l` long format
- `-a` include hidden files
- `-h` used with the long format to pretty print file sizes
- `-1` force input to be one per line

_Example_: `ls -lah ~/Documents/gsandf`

---

### Concatination

```bash
$ cat <file>
```

Prints out a file to STDOUT.
If more than one file is supplied, concatinates and prints files.

_Example_: `cat ./Readme.md`

---

### Less (is more)

```bash
$ less <file>
```

Opens a file, files, or other output in an interactive view.
Can be used to scroll through files in a view-only mode (similar to vim).
Man pages use this by default.

Helpful Navigation Commands:

- `q` quit
- `j` scroll down a line
- `k` scroll up a line
- `d` scroll down a page
- `u` scroll up a page
- `/` search for a word
    - `n` next result
    - `p` previous result
- `:` command mode
    - `n` next file
    - `p` previous file

---

### Working Directory

```bash
$ pwd
```

Gets the full path of the current working directory

---

### Move File(s)

```bash
$ mv <source> <destination>
```

Moves a source file from one location to another.
Can also be used to rename a file.

_Example_: `mv file.txt ~/Documents`

---

### Copy File(s)

```bash
$ cp <source> <target>
```

Copies a source file.

_Example_: `cp file.txt newFile.txt`

---

### Remove File(s)

```bash
$ rm <source>
```

Removes a source file (or directory).
Can be passed multiple source files.

Helpful Options:

- `-f` remove without asking for permissions
- `-r` removes files and directories

_Example_: `rm file.txt`

---

### File Pattern Searcher

```bash
$ grep <string> <source>
```

Searches the input (file, directory, output) for
a supplied string.
Alternatives include _awk_, _sed_, _ag_.

Helpful Options:

- `-r` recursive search
- `-n` display resultswith line number
- `-l` display on the files with matches
- `-i` case insensitive searching
- `-A` show number of lines before match
- `-B` show number of lines after match
- `--color=always` highlight the search results

_Example_: `grep -rl "moment" src/components`

---

### Word Count

```bash
$ wc <source>
```

Returns the number of words (or lines) in the supplied source.
The source can be a file or output from the terminal.

Helpful Options:

- `-l` return the number of lines
- `-c` return the number of bytes

_Example_: `wc -l npm-debug.log`

---

### Create a New File

```bash
$ touch <filename>
```

Creates a new file, given a name.

_Example_: `touch src/components/Header.js`

---

### Make a New Directory

```bash
$ mkdir <dirname>
```

Creates a new directory, given a name.

_Example_: `mkdir client`

---

### Display a File

```bash
$ tail <source>
```

Displays the last few lines of a file.
Can be used to "watch" the output of a file (useful for logging).

Helpful Options:

- `-f` keep displaying new lines for the file
- `-n` specify the number of lines to show

_Example_: `tail -f npm-debug.log`

## GIT Commands


### Status

```bash
$ git status
```

Get the current status (recursivley) for this directory.
Includes changed files, staged files, the current branch.
Optionally shows ignored files.

Helpful Options:

- `-s` shorthand status (helpful with `wc`)
- `--ignored` show files that are being ignored by GIT

_Example_: `git status -s`

---

### Difference

```bash
$ git diff
```

Determines the difference between the current state (or supplied state)
and the head (or supplied state).

Helpful Options:

- `--name-only` only show the file names for the files that are different

_Example_: `git diff --name-only`

---

### Log

```bash
$ git log
```

Shows the commit logs.
Can be passed a file to see the git history for it specifically.
Highly customizable. Can mimic most screens in Bitbucket.

Helpful Options:

- `--oneline` show the commits in a short one-line format
- `--graph` show the commits in a branch graph
- `-n` only show a supplied number of commits
- `--format` specify how the commits should be displayed

_Example_: `git log --oneline -n 10`

## Helpful Tips


### Pipe

```bash
$ [command-1] | [command-2]
```

Pass the output of a command as a parameter for a following command.
Multiple commands can be chained together like this.

_Example_: `git diff --name-only | wc -l`

^ Gets the number of files currently changed

---

### Copy to Clipboard

```bash
$ pbcopy [output]
```

**MAC ONLY** copies supplied output to the system clipboard.
Useful with pipes.

_Example_: `cat package.json | pbcopy`

^ Copies the package.json file to the clipboard

---

### Tab

Pressing tab will auto-complete file and directory names.
Pressing tab twice will return a list of possible matches.
Some commands will also auto-complete.

In ZSH, auto-complete list will be interactive.

---

### Up

Pressing up will enter the last entered command.
You can continue to press up to see more search results.
You can press down to go down the list.

---

### Last Command

```bash
$ !!
```

Two exclamation-marks are used to reference the last command typed in.
This is useful for running a command, seeing the output, and then
running it again with extra options or follow-up commands.

Can be configured to auto-complete the last command.

---

### Multiple Commands

```bash
$ [command-1] && [command-2]
```

Run a command and then run another one after it's complete.
Can run as many commands as needed on one line.
This is syncronous.

---

### Esc

Exits "enter" mode and goes into "command" mode.
Similar to emacs and vim.
Allows for faster traversal of the current line.
Can be used to search previously entered commands.

Defaults to emacs key bindings. Can be set to use vim bindings.

---

### Manual

```bash
$ man [command]
```

Shows the manual (if there is one) for a given command.
Shows all the posibile options.

---

### Insert Previous Arguments

```bash
$ !$
```

An exclamation followed by a dollar sign will insert the arguments
from the last command. This works like the double exclamation mark.
Can be set to auto-expand.

Adding two exclamation marks will do the arguments for the second-
to-last command.

---

### Reverse Search

Hitting `control-r` will enter into reverse search mode.
As you enter text, the shell will show the last command that
matches that search. You can hit `control-r` to see more results.

Press enter to run the command, or an arrow key to insert it in the
shell without running.

Very useful way to run long commands you don't fully remember.

---

### Search and Replace

```bash
$ ^search^replace
```

Allows you to run the last command, but with by replacing part of it.
Helpful if you mis-type something or want to perform a different
operation on the same file.

_Example_:

```bash
$ cat clint/src/components/grid/GridHeader.js
-bash: cat: clint: No such file or directory

$ ^clint^client
```
---

### Argument Expand

Using `{` and `}`, you can write long, but repetitive commands
in a much simpler format. All comma separated items in the braces
will be auto-expanded to a command for each item.

Useful for creating or removing multiple files.

_Example_:

```bash
$ touch letters/{a,b,c}.txt
```

Is equivalent to:

```bash
$ touch letters/a.txt && touch letters/b.txt && touch letters/c.txt
```

---

### Wildcard

An astrix (`*`) can be used as a wildcard.
Helpful if you don't want to fully type out a filename.

_Example_:

```bash
$ cat presidents/abraham*
```

---

### User Config Files

Shells allow for a configuration file that the user can create
and use to define custom variables, functions, and custom settings.

For BASH, the file is `~/.bash_profile`.
For ZSH, the file is `~/.zshrc`.
Other hipster shells have a similar file.

Useful for setting aliases for commonly run programs.

_Example_:

```bash
alias dc="docker-compose"

alias search="grep -rn --color=always"
```

